import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

//****************************** CLASS HOME ************************************
class HomePage extends StatelessWidget {
  final String query = r"""
                    query GetContinent($code : String!){
                      continent(code:$code){
                        name
                        countries{
                          name
                        }
                      }
                    }
                  """;
  /*
   query GetContinent($code : String!){                     // GET QUERY
                      continent(code:$code){                // parameter
                        name
                        countries{
                          name
                        }
                      }
                    }
   */

  //****************************** ROOT WIDGETS ********************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text("GraphlQL Client"),
      ),

      body: Query(

        options: QueryOptions(
            document: query,        // this is the query string you just created
            variables: <String, dynamic>{"code": "AF"},  //ARGUMENTS QUERY
            //variables: {},
            pollInterval: 10,                             //REQUEST INTERVAL
        ),

        // Just like in apollo refetch() could be used to manually trigger a refetch
        // while fetchMore() can be used for pagination purpose
        builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore}) {

          if (result.data == null) {
            //return Text("No Data Found !");
            return Text(result.errors.toString());
          }

          if (result.loading) {
            return Center(child: CircularProgressIndicator());
          }

          /*
           // it can be either Map or List
            List repositories = result.data['viewer']['repositories']['nodes'];

            return ListView.builder(
              itemCount: repositories.length,
              itemBuilder: (context, index) {
                final repository = repositories[index];

                return Text(repository['name']);
            });
           */
          
          return ListView.builder(
            itemCount: result.data['continent']['countries'].length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title:
                    Text(result.data['continent']['countries'][index]['name']),
              );
            },
          );
           /*
          return ListView.builder(
            itemCount: result.data['continents']['countries'].length,
            /*
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title:
                Text(result.data['continents']['countries'][index]),
              );
            },
            
             */
          );
          */
        },

      ),

    );
  }
}
//******************************************************************************
