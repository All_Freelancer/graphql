import 'package:flutter/material.dart';
import 'package:flutter_graphql_app/src/home.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

//***************************** CLASS ROOT MAIN ********************************
void main() {
  runApp(MaterialApp(title: "GQL App", home: MyApp()));
}

//****************************** CLASS MY APP **********************************
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink =
    HttpLink(uri: "https://countries.trevorblades.com/");

    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: httpLink as Link,
        cache: OptimisticCache(
          dataIdFromObject: typenameDataIdFromObject,
        ),
      ),
    );

    return GraphQLProvider(
      child: HomePage(),
      client: client,
    );
  }
}
//******************************************************************************
